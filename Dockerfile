FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y \
    jackd \
    jack-tools && \
    apt-get remove qjackctl -y && \
    apt-get autoremove -y
